from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image
from django.conf import settings
from uuid import uuid4


def generateUUID():
    return str(uuid4())


class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    likes = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, related_name="post_like"
    )

    def __str__(self):
        return self.title

    @property
    def get_absolute_url(self):
        return reverse("post-detail", kwargs={"pk": self.pk})

    class Meta:
        ordering = ["-date_posted"]

    def total_likes(self):
        return self.likes.count()


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="", default="default.JPG")

    def __str__(self):
        return str(self.user)

    # def save(self):s

    #     img = Image.open(self.image.path)

    #     if img.height > 50 or img.width > 50:
    #         output_size = (50, 50)
    #         img.thumbnail(output_size)
    #         img.save(self.image.path)
    #     super(Profile, self).save()

