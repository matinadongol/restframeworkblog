from django.contrib import admin
from .models import Post, Profile
from .forms import UserRegisterForm

admin.site.register(Post)

admin.site.register(Profile)

