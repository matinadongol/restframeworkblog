from django.shortcuts import render, reverse, redirect
from django.contrib.auth.hashers import make_password
from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    UpdateView,
    DeleteView,
    FormView,
    TemplateView,
    RedirectView,
)
from django.urls import reverse_lazy
from django.http import Http404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.utils.http import is_safe_url
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import (
    REDIRECT_FIELD_NAME,
    login as auth_login,
    logout as auth_logout,
)
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView, RedirectView
import json

# from django.contrib.auth.views import login as django_login_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from blog.serializer import PostSerializer, ProfileSerializer, UserSerializer
from blog.models import *
from blog.models import Post, Profile
from blog.forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView


def home(request):
    context = {"post": Post.objects.all()}
    return render(request, "blog/home.html", context)


class PostListView(ListView):
    model = Post
    template_name = "blog/home.html"
    context_object_name = "posts"
    ordering = ["-date_posted"]


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    template_name = "blog/post_create.html"
    fields = ["title", "content"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class PostAPIView(APIView):
    def get(self, request):
        blog = Post.objects.all()
        serializer = PostSerializer(blog, many=True)
        return Response(serializer.data, status=200)

    def post(self, request):
        data = request.data
        data["author"] = request.data.user.id
        print("*********************")
        print(data)
        serializer = PostSerializer(data=data)
        print(serializer)
        if serializer.is_valid():
            print("yes valid")
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)


class PostDetailView(DetailView):
    model = Post
    template_name = "blog/detail.html"
    context_object_name = "post"

    # def post_detail(request, id):
    #     post = get_object_or_404(Post, id=id)
    #     is_liked = False
    #     if post.likes.filter(id=request.user.id).exists():
    #         is_liked = True
    #     context = {
    #         "post": post,
    #         "is_liked": is_liked,
    #         "total_likes": post.total_likes(),
    #     }
    #     return render(request, "blog/detail.html", context)


class PostDetailAPIView(APIView):
    def get_object(self, pk):
        try:
            instance = Post.objects.get(pk=pk)
            return instance
        except Post.DoesNotExist as e:
            return Response({"error": "Given post not found"}, status=404)

    def get(self, request, pk=None):
        instance = Post.objects.get(pk=pk)
        serializer = PostSerializer(instance)
        return Response(serializer.data)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ["title", "content"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostUpdateAPIView(APIView):
    def get_object(self, pk):
        try:
            instance = Post.objects.get(pk=pk)
            return instance
        except Post.DoesNotExist as e:
            return Response({"error": "Given post doesnot exist"}, status=404)

    def put(self, request, pk=None):
        data = request.data
        print(data)
        instance = self.get_object(pk)
        serializer = PostSerializer(instance, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=200)
        return Response(serializer.errors, status=400)


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = reverse_lazy("blog-home")

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteAPIView(APIView):
    def get_object(self, pk):
        try:
            instance = Post.objects.get(pk=pk)
            return instance
        except Post.DoesNotExist as e:
            return Response({"error": "Given post doesnot exist"}, status=404)

    def delete(self, request, pk=None):
        instance = self.get_object(pk)
        instance.delete()
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)
        return Response(serializer.data, status=204)


class RegisterTempView(FormView):
    template_name = "blog/register.html"
    form_class = UserRegisterForm
    success_url = reverse_lazy("login")


class RegisterAPIView(APIView):
    def get(self, request):
        prof = User.objects.all()
        serializer = UserSerializer(prof, many=True)
        return Response(serializer.data, status=200)

    def post(self, request):
        data = request.data
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
        else:
            print("not valid")
        return Response(serializer.errors, status=404)


class RegisterListView(ListView):
    model = Profile
    template_name = "blog/home.html"
    context_object_name = "blog"
    ordering = ["author_id"]


class loginv(LoginRequiredMixin):
    pass


class Loginv(LoginView):
    form_class = AuthenticationForm
    template_name = "blog/login.html"
    success_url = reverse_lazy("blog-home")

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("profile")
        else:
            return redirect("login")


class ProfileTempView(ListView):
    model = Profile
    template = "blog/profile.html"
    context_object_name = "blog"

    def get(self, request):
        blog = Post.objects.filter(author=request.user.id)
        context = {"blog": blog}
        return render(request, "blog/profile.html", context)


class ProfileAPIView(APIView):
    def get(self, request):
        pro = Profile.objects.all()
        serializer = ProfileSerializer(pro, many=True)
        return Response(serializer.data, status=200)

    def post(self, request):
        data = request.data
        serializer = ProfileSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
        else:
            print("not valid")
        return Response(serializer.errors, status=404)

    # def post(self, request):
    #     data = request.data
    #     serializer = ProfileSerializer(data=data)
    #     u_form = UserUpdateForm(request.POST, instance=request.user)
    #     p_form = ProfileUpdateForm(
    #         request.POST, request.FILES, instance=request.user.profile
    #     )
    #     if u_form.is_valid() and p_form.is_valid():
    #         u_form.save()
    #         p_form.save()
    #         messages.success(request, f"Your account had been updated")
    #         # return Response(serializer.data, status=400)
    #     else:
    #         u_form = UserUpdateForm(instance=request.user)
    #         p_form = ProfileUpdateForm(instance=request.user.profile)
    #         # return Response(serializer.data, status=400)

    #     blog = Post.objects.filter(author=request.user.id)

    #     context = {"u_form": u_form, "p_form": p_form, "blog": blog}
    #     print(context)
    #     return render(request, "blog/profile.html", context)


class ProfileUpdateView(FormView):
    model = Profile
    fields = ["user", "image"]
    form_class = ProfileUpdateForm
    template_name = "blog/profileupdate.html"

    def get_object(self, pk):
        print("*****************************")
        try:
            instance = Profile.objects.get(pk=pk)
            return instance
        except Profile.DoesNotExist as e:
            return Response({"error": "Given profile doesnot exist"}, status=404)

    def post(self, request):
        if request.method == "POST":
            # user = request.user.profile
            u_form = UserUpdateForm(request.POST, instance=request.user)
            p_form = ProfileUpdateForm(
                request.POST, request.FILES, instance=request.user.profile
            )
            if u_form.is_valid() and p_form.is_valid():
                # user.user = request.POST["user"]
                # user.image = request.POST["image"]
                u_form.save()
                p_form.save()
                # user.save()
                messages.success(request, f"Your account had been updated")
                return redirect("profile")
        else:
            u_form = UserUpdateForm(instance=request.user)
            p_form = ProfileUpdateForm(instance=request.user.profile)
            # return Response(serializer.data, status=400)

        context = {"u_form": u_form, "p_form": p_form}
        return render(request, "blog/profileupdate.html", context)


class ProfileUpdateAPIView(APIView):
    def get_object(self, pk):
        try:
            instance = Profile.objects.get(pk=pk)
            return instance
        except Profile.DoesNotExist as e:
            return Response({"error": "Given profile doesnot exist"}, status=404)

    def put(self, request, pk=None):
        data = request.data
        proinstance = self.get_object(pk)
        proserializer = ProfileSerializer(proinstance, data=data)
        useinstance = self.get_object(pk)
        useserializer = UserSerializer(useinstance, data=data)
        serializer = {"proserializer", "useserializer"}
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=200)
        return Response(serializer.errors, status=400)


# class ProfileUpdateListView(ListView):
#     model = Profile
#     template_name = "blog/profileupdate.html"
#     context_object_name = "blog"


def logout_view(request):
    logout(request)
    return redirect("login")


def like_post(request):
    print(request.POST.get("post_id"))
    post = get_object_or_404(Post, id=request.POST.get("post_id"))
    is_liked = False
    if post.likes.filter(id=request.user.id).exists():
        print("False")
        post.likes.remove(request.user)
        is_liked = False
        # return HttpResponseRedirect(post.get_absolute_url)
    else:
        print("Trues")
        post.likes.add(request.user)
        is_liked = True
        # return HttpResponseRedirect(post.get_absolute_url)
    print(post)
    # print(total_likes)
    context = {"post": post, "is_liked": is_liked, "total_likes": post.total_likes()}
    return render(request, "blog/detail.html", context)
