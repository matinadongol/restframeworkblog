from django.urls import path, include
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns

# from . import views
from blog.views import (
    PostListView,
    PostCreateView,
    PostDetailView,
    PostUpdateView,
    PostDeleteView,
    RegisterTempView,
    Loginv,
    ProfileTempView,
    ProfileUpdateView,
)
from blog import views
from rest_framework.routers import DefaultRouter


urlpatterns = [
    path("post/", PostListView.as_view(), name="blog-home"),
    path("post/new/", PostCreateView.as_view(), name="post-create"),
    path("post/<int:pk>/", PostDetailView.as_view(), name="post-detail"),
    path("post/like/", views.like_post, name="post_like"),
    path("post/update/<int:pk>/", PostUpdateView.as_view(), name="post-update"),
    path("post/delete/<int:pk>/", PostDeleteView.as_view(), name="post-delete"),
    path("register/", RegisterTempView.as_view(), name="register"),
    # path("login/", LoginView.as_view(), name="login"),
    path("logout/", views.logout_view, name="logout"),
    path("login/", Loginv.as_view(), name="login"),
    path(
        "profile/",
        ProfileTempView.as_view(template_name="blog/profile.html"),
        name="profile",
    ),
    path(
        "profile/update/<int:pk>/", ProfileUpdateView.as_view(), name="profile-update"
    ),
    path("api/", include("blog.api_urls")),
]

urlpatterns = format_suffix_patterns(urlpatterns)

