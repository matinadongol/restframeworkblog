from django.urls import path
from blog.views import *


urlpatterns = [
    path("post/", PostAPIView.as_view(), name="apipost"),
    path("post/<int:pk>/", PostDetailAPIView.as_view(), name="apipostdetail"),
    path("post/update/<int:pk>/", PostUpdateAPIView.as_view(), name="apipostupdate"),
    path("post/delete/<int:pk>/", PostDeleteAPIView.as_view()),
    path("register/", RegisterAPIView.as_view(), name="apiregister"),
    path("profile/<int:pk>/", ProfileAPIView.as_view(), name="apiprofile"),
    path(
        "profile/update/<int:pk>/",
        ProfileUpdateAPIView.as_view(),
        name="apiprofileupdate",
    ),
]

