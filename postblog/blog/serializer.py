from rest_framework import serializers
from blog.models import Post, Profile
from rest_framework.serializers import ValidationError
from .models import *
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = "__all__"


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = "__all__"

    def update(self, instance, validated_data):
        profile_data = validated_data.pop("profile")
        profile = instance.profile

        instance.user = validated_data.get("user", instance.user)
        instance.image = validated_data.get("image", instance.image)
        instance.save()

        profile.user = profile_data.get("user", profile.user)
        profile.image = profile_data.get("image", profile.image)
        profile.save()

        return instance


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ("username", "email", "password")

    def create(self, validated_data):
        print(validated_data)
        user = User(email=validated_data["email"], username=validated_data["username"])
        user.set_password(validated_data["password1"])
        print(user)
        user.save()
        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop("profile")
        profile = instance.profile

        instance.user = validated_data.get("user", instance.user)
        instance.image = validated_data.get("image", instance.image)
        instance.save()

        profile.user = profile_data.get("user", profile.user)
        profile.image = profile_data.get("image", profile.image)
        profile.save()

        return instance
